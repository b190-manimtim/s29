// OR Operator
db.users.find({
    $or: [
        { firstName: { $regex: "s", $options: "$i" } },
        { lastName: { $regex: "t", $options: "$i" } },

    ]

}, {
    firstName: 1,
    lastName: 1,
    _id: 0
});

// AND Operator, gte operator
db.users.find({
    $and: [
        { department: "HR" },
        { age: { $gte: 30 } }
    ]
});

// AND, lte operator
db.users.find({
    $and: [
        { firstName: { $regex: "e", $options: "$i" } },
        { age: { $lte: 30 } }
    ]
});